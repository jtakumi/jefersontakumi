//
//  PushRespository.swift
//  Desafio iOS
//
//  Created by Magically02 on 29/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import ObjectMapper

class PullDataItem: Mappable {
    
    var title : String = ""
    var body : String = ""
    var html_url : String = ""
    var user_data : UserData!
    var created_at : String = ""
    
    required init?(map: Map) {
        
    }
    
    init()
    {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        html_url <- map["html_url"]
        user_data <- map["user"]
        created_at <- map["created_at"]
    }
}
