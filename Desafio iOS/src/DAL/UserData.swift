//
//  UserDatas.swift
//  Desafio iOS
//
//  Created by Magically02 on 28/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import ObjectMapper

class UserData: Mappable {
    
    var login : String!
    var avatar_url : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        avatar_url <- map["avatar_url"]
    }
}
