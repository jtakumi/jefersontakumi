//
//  Repositories.swift
//  Desafio iOS
//
//  Created by Magically02 on 28/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import ObjectMapper

class RepositoriesGit: Mappable {
    
    var incomplete_results: Bool = true
    var items : [RepositoryItem]!
    
    required init?(map: Map) {
        
    }
    
    init()
    {
        
    }
    
    func mapping(map: Map) {
        incomplete_results <- map["incomplete_results"]
        items <- map["items"]
    }
}
