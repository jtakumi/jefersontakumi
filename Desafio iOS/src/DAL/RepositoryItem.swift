//
//  RepositoryItem.swift
//  Desafio iOS
//
//  Created by Magically02 on 27/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import ObjectMapper

class RepositoryItem: Mappable {
    
    var id : Int = 0
    var name_project : String = ""
    var user_data : UserData!
    var description_project : String = ""
    var stars : Int = 0
    var forks : Int = 0
    var open_issues : Int = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name_project <- map["name"]
        user_data <- map["owner"]
        description_project <- map["description"]
        stars <- map["watchers"]
        forks <- map["forks"]
        open_issues <- map["open_issues"]
        
    }
}
