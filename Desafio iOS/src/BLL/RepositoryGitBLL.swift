//
//  RepositoryGitBLL.swift
//  Desafio iOS
//
//  Created by Magically02 on 29/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import Foundation
import Alamofire

/*
 Parametros para o search
 */
enum Sort: String {
    case forks = "forks"
    case stars = "stars"
    case updated = "updated"
}

//Default desc
enum Order: String {
    case asc = "asc"
    case desc = "desc"
}

/*
 Parametros para o pull request
 */

//Default open
enum State: String {
    case open = "open"
    case closed = "closed"
    case all = "all"
}

//Default created
enum SortPull: String {
    case created = "created"
    case updated = "updated"
    case popularity = "popularity"
    case longrunning = "long-running"
}

//Default desc
enum Direction: String {
    case asc = "asc"
    case desc = "desc"
}

class RepositoryGitBLL {
    
    public func requestRepository(page : Int, sort: Sort, success: @escaping (_ result: RepositoriesGit) -> Void, failure: @escaping () -> Void)
    {
        let parameters: Parameters = ["q": "language:Java",
                                      "sort": sort.rawValue,
                                      "page": page]
        DSNetwork.shared.requestGet(requestUrl: gitHost + "search/repositories", parameters: parameters, success: success, failure: failure)
    }
    
    public func requestPull(owner : String, repositoryName: String, success: @escaping (_ result: [PullDataItem]) -> Void, failure: @escaping () -> Void)
    {
        DSNetwork.shared.requestGetArray(requestUrl: gitHost + "repos/" + owner + "/" + repositoryName + "/pulls", parameters: [:], success: success, failure: failure)
    }
}
