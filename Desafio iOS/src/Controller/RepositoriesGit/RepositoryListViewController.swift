//
//  RepositoryListViewCellTableViewController.swift
//  Desafio iOS
//
//  Created by Magically02 on 27/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class RepositoryListViewController: UITableViewController {
    
    static var identifier = "repositoryListView"
    private var repositoriesItem = [RepositoryItem]()
    private var page = 1
    private var sort = Sort.stars
    private let repositoryGitBLL = RepositoryGitBLL()
    var pullListView : PullListViewController!
    let progressHUD = UILoadViewController(text: "Carregando ...")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Github JavaPop"
        
        self.view.addSubview(progressHUD)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(repositoriesItem.count == 0)
        {
            requestRepository()
        }
    }
    
    func requestRepository()
    {
        progressHUD.show()
        self.tableView.isScrollEnabled = false;
        repositoryGitBLL.requestRepository(page: page, sort: sort, success: {
            repositories in
            let repositoryGit = repositories as RepositoriesGit
            if(!repositoryGit.incomplete_results && (self.page * 30) <= 1000)
            {
                for itemAdd in repositoryGit.items {
                    self.repositoriesItem.append(itemAdd)
                }
                self.page += 1
            }
            self.tableView.reloadData()
            self.progressHUD.hide()
            self.tableView.isScrollEnabled = true;
        }, failure: {
            self.progressHUD.hide()
            self.tableView.isScrollEnabled = true;
        })
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            self.requestRepository()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(pullListView == nil) {
            pullListView = (self.storyboard?.instantiateViewController(withIdentifier: "pullListView"))! as! PullListViewController
        }
        if let repositoryItem = repositoriesItem[indexPath.row] as RepositoryItem! {
            pullListView.owner = repositoryItem.user_data.login
            pullListView.repositoryName = repositoryItem.name_project
            pullListView.open_issues = repositoryItem.open_issues
            //pullListView.reset()
            self.navigationController?.pushViewController(pullListView, animated: true)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoriesItem.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ItemRepositoryListCell = tableView.dequeueReusableCell(withIdentifier: "repListCell", for: indexPath) as! ItemRepositoryListCell
        cell.fillCell(repositoriesItem[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }

}
