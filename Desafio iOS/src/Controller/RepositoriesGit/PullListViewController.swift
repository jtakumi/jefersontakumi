//
//  PushListViewController.swift
//  Desafio iOS
//
//  Created by Magically02 on 29/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import UIKit

class PullListViewController: UITableViewController {

    static var identifier = "pushListView"
    private var pullDataItens = [PullDataItem]()
    var owner : String!
    var repositoryName : String!
    var open_issues : Int = 0
    private let repositoryGitBLL = RepositoryGitBLL()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.title = repositoryName
        
        requestPull()
    }
    
    func reset()
    {
        pullDataItens = [PullDataItem]()
        self.tableView.reloadData()
    }
    
    func requestPull()
    {
        //progressHUD.show()
        self.tableView.isScrollEnabled = false;
        repositoryGitBLL.requestPull(owner: self.owner, repositoryName: self.repositoryName, success: {
            repositories in
            let pullDatas = repositories 
            if(pullDatas.count > 0) {
                self.pullDataItens = pullDatas
            }
            else
            {
                self.pullDataItens = [PullDataItem]()
            }
            
            self.tableView.reloadData()
            //self.progressHUD.hide()
            self.tableView.isScrollEnabled = true;
        }, failure: {
            //self.progressHUD.hide()
            self.tableView.isScrollEnabled = true;
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullDataItens.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PullListCell = tableView.dequeueReusableCell(withIdentifier: "pullListCell", for: indexPath) as! PullListCell
        cell.fillCell(pullDataItens[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if let item = pullDataItens[indexPath.row] as PullDataItem! {
           UIApplication.shared.openURL(URL(string: item.html_url)!)
        }
    }

}
