//
//  ItemRepositoryListCell.swift
//  Desafio iOS
//
//  Created by Magically02 on 28/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Font_Awesome_Swift

class ItemRepositoryListCell: UITableViewCell {

    @IBOutlet weak var lblRepositoryTitle : UILabel!
    @IBOutlet weak var lblRepositoryDescription : UILabel!
    @IBOutlet weak var lblForksDescription : UILabel!
    @IBOutlet weak var lblStarsDescription : UILabel!
    @IBOutlet weak var imgAvartar : UIImageView!
    @IBOutlet weak var lblUserName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func fillCell(_ repositoryItem : RepositoryItem) {
        lblRepositoryTitle.text = repositoryItem.name_project
        lblRepositoryDescription.text = repositoryItem.description_project
        lblForksDescription.setFAText(prefixText: "", icon: FAType.FACodeFork, postfixText: String(repositoryItem.forks), size: 12, iconSize: 20)
        lblStarsDescription.setFAText(prefixText: "", icon: FAType.FAStar, postfixText: String(repositoryItem.stars), size: 12, iconSize: 20)
        lblUserName.text = repositoryItem.user_data.login
        if(repositoryItem.user_data.avatar_url.isEmpty)
        {
            imgAvartar.setFAIconWithName(icon: FAType.FAUser, textColor: .lightGray, orientation: UIImageOrientation.down, backgroundColor: .black, size: CGSize(width: 45, height: 45))
        }
        else
        {
            do {
                let url = URL(string: repositoryItem.user_data.avatar_url)
                let data = try Data(contentsOf: url!)
                imgAvartar.image = UIImage(data: data)
            }
            catch{
                print(error)
            }
        }
    }    

    /*override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }*/

}
