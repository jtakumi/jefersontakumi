//
//  PullListCell.swift
//  Desafio iOS
//
//  Created by Magically02 on 29/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import UIKit
import FontAwesome_swift
import Font_Awesome_Swift

class PullListCell: UITableViewCell {

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgAvartar : UIImageView!
    @IBOutlet weak var lblUserName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func fillCell(_ pullDataItem : PullDataItem) {
        lblTitle.text = pullDataItem.title
        lblDescription.text = pullDataItem.body
        lblDate.text = Util.formaterData(date: pullDataItem.created_at)
        lblUserName.text = pullDataItem.user_data.login
        lblUserName.text = pullDataItem.user_data.login
        if(pullDataItem.user_data.avatar_url.isEmpty)
        {
            imgAvartar.setFAIconWithName(icon: FAType.FAUser, textColor: .lightGray, orientation: UIImageOrientation.down, backgroundColor: .black, size: CGSize(width: 45, height: 45))
        }
        else
        {
            do {
                let url = URL(string: pullDataItem.user_data.avatar_url)
                let data = try Data(contentsOf: url!)
                imgAvartar.image = UIImage(data: data)
            }
            catch{
                print(error)
            }
        }
    }

}
