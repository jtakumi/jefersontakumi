//
//  DSNetwork.swift
//  Desafio iOS
//
//  Created by Magically02 on 29/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class DSNetwork {
    
    static let shared = DSNetwork()
    static let sessionManager = Alamofire.SessionManager.default

    func requestGet<T : Mappable>(requestUrl : String, parameters: Parameters, success: @escaping (_ result: T) -> Void, failure: @escaping () -> Void) {
        //Alamofire.request(requestUrl).validate().responseObject { (response: DataResponse<T>) in
        DSNetwork.sessionManager.request(requestUrl, method: .get, parameters: parameters).validate().responseObject { (response: DataResponse<T>) in
            switch response.result {
                case .success:
                    if let jsonData : T = response.result.value {
                        success(jsonData)
                    } else {
                        failure()
                    }
                case .failure:
                    failure()
            }
        }
    }
    
    func requestGetArray<T : Mappable>(requestUrl : String, parameters: Parameters, success: @escaping (_ result: [T]) -> Void, failure: @escaping () -> Void) {
        DSNetwork.sessionManager.request(requestUrl, method: .get).validate().responseJSON { response in
            switch response.result {
                case .success:
                    let jsonData = Mapper<T>().mapArray(JSONArray: response.result.value as! [[String : Any]])
                    if jsonData.count > 0 {
                        success(jsonData)
                    } else {
                        failure()
                    }
                case .failure:
                    failure()
            }
        }
    }
}
