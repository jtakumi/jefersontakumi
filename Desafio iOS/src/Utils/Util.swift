//
//  Util.swift
//  Desafio iOS
//
//  Created by Magically02 on 28/03/18.
//  Copyright © 2018 Nenhum. All rights reserved.
//

import UIKit

class Util {
    
    public static func cornerImage(_ imageView : UIImageView){
        imageView.layer.cornerRadius = imageView.bounds.height/2
    }
    
    public static func formaterData(date : String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        let formatterOriginal = DateFormatter()
        formatterOriginal.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z"
        
        let cDate = formatterOriginal.date(from: date)
        return formatter.string(from: cDate!)
    }
}
